FROM node:14-alpine As development

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .
RUN npm i -g @nestjs/cli
RUN npm i -g rimraf
RUN npm run build
EXPOSE 4000
CMD ["node", "dist/main.js"]
