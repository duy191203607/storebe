CREATE DATABASE  IF NOT EXISTS `store` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `store`;
-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: store
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_4c8f96ccf523e9a3faefd5bdd4` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'duydk2001@gmail.com','$2b$10$avI0vBCM1d5cShNAdu/DheXs4SsuSPjD0O38rJHcYbRntj8bM/.Fu');
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `name` varchar(1000) NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES ('Whisky',1),('Cognac',2),('Vodka',3),('Rượu mùi',4),('Champane',5),('Bia',9),('Rượu ta',10),('Thời trang',11),('Đồ điện tử',12),('Rau củ quả',13),('Nước hoa',14),('Đồ gia dụng',15);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order` (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `price` float NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(200) NOT NULL,
  `createdAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updatedAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `accountId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_8cb9cecbc8b09bf60c71f7a9680` (`accountId`),
  CONSTRAINT `FK_8cb9cecbc8b09bf60c71f7a9680` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1,3221,'nDuy','0969317521','duydk2001@gmail.com','CT6A, Xa La, Ha Dong, Ha Noi, Viet Nam','2022-04-23 14:21:35.334502','2022-04-23 14:21:35.334502',1),(2,316,'nDuy','0969317521','duydk2001@gmail.com','CT6A, Xa La, Ha Dong, Ha Noi, Viet Nam','2022-04-24 05:17:21.137061','2022-04-24 05:17:21.137061',1);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_product`
--

DROP TABLE IF EXISTS `order_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_product` (
  `id` varchar(36) NOT NULL,
  `quantity` smallint NOT NULL,
  `productId` int DEFAULT NULL,
  `orderId` smallint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_073c85ed133e05241040bd70f02` (`productId`),
  KEY `FK_3fb066240db56c9558a91139431` (`orderId`),
  CONSTRAINT `FK_073c85ed133e05241040bd70f02` FOREIGN KEY (`productId`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_3fb066240db56c9558a91139431` FOREIGN KEY (`orderId`) REFERENCES `order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_product`
--

LOCK TABLES `order_product` WRITE;
/*!40000 ALTER TABLE `order_product` DISABLE KEYS */;
INSERT INTO `order_product` VALUES ('26371d3a-2041-479d-8668-6d7c8e7f9501',1,80,1),('2de3a49f-7ee3-4156-ac95-a29d962bb739',2,115,1),('360cb089-f8f3-410e-b0c6-80add1f5132d',1,88,2),('3ec9f1f5-50f7-497e-8553-84055cccb6fd',1,128,1),('5dbe32c3-4bc7-48fb-99f0-7a821598d193',1,124,1),('62d733f0-758a-4c6e-b034-6e3d60d2f776',1,117,1),('647956e8-63ca-4569-a97f-ba8a98305711',1,87,2),('6ad80900-d30b-4009-904f-6a485e7f6604',1,123,1),('6b325101-a0ab-4364-8e34-6e1d7b5203ef',1,121,1),('6ffd8fbe-5f45-49dd-9b56-d7ba0f3b6901',1,45,1),('7453c3c0-c483-4fc2-853a-46c52e210313',1,129,1),('7bb10bfc-1a0e-4427-a655-66818509d7af',1,120,1),('8559d16e-f513-4460-a652-ec3ab54e8914',1,168,1),('8675608b-2473-41a7-82fd-5f7ce37ef37c',1,130,1),('88f4a0f2-f9cb-441b-8d75-8889772fccdf',1,86,2),('900bcb1e-bb0d-4d2c-9a94-9f7765e8af7f',1,132,1),('a2cfa578-e80f-40e3-a672-6eac927475fa',1,83,2),('b0877e59-1a8b-4665-8ad6-d785b70dbac8',1,131,1),('c79e984a-ac1a-4678-b44c-d3e1b59f39cc',1,134,1),('c8aecb29-b89d-4c30-bd79-109be27adae2',1,116,1),('cba8bc76-ec90-4cdb-a016-aa032b7de527',1,118,1),('d16eef2f-a9f2-4e50-8eab-99fb9f13f5a2',1,81,1),('d1d59b9f-69e2-4a7d-9e6f-7e5a9e1010f3',1,127,1),('dcb260b7-7ce2-4176-ac6f-c0488a3c93ce',1,89,2),('df01936f-1845-4a8d-b229-a82d238692eb',1,125,1),('e757cffd-5814-4f7d-bda6-7c3d66dc353e',1,87,1),('ebbd5de8-d5a0-477c-b9cc-0019bcf9c923',1,69,1),('ec5aed9f-1217-4d9b-b37d-8edcdaa788de',1,133,1),('f0bb61e1-d956-428c-acac-1f0534ca1a1f',1,126,1);
/*!40000 ALTER TABLE `order_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderProduct`
--

DROP TABLE IF EXISTS `orderProduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orderProduct` (
  `id` varchar(36) NOT NULL,
  `quantity` smallint NOT NULL,
  `productId` int DEFAULT NULL,
  `orderId` smallint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_7d18d1fafedc1b39c5f2c40c03d` (`productId`),
  KEY `FK_16ed2dd2152e905b788b4302180` (`orderId`),
  CONSTRAINT `FK_16ed2dd2152e905b788b4302180` FOREIGN KEY (`orderId`) REFERENCES `order` (`id`),
  CONSTRAINT `FK_7d18d1fafedc1b39c5f2c40c03d` FOREIGN KEY (`productId`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderProduct`
--

LOCK TABLES `orderProduct` WRITE;
/*!40000 ALTER TABLE `orderProduct` DISABLE KEYS */;
/*!40000 ALTER TABLE `orderProduct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) NOT NULL DEFAULT '',
  `inventory` int NOT NULL DEFAULT '0',
  `price` float NOT NULL DEFAULT '0',
  `categoryId` int DEFAULT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ff0c0301a95e517153df97f6812` (`categoryId`),
  CONSTRAINT `FK_ff0c0301a95e517153df97f6812` FOREIGN KEY (`categoryId`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Glenfiddich 12',32,51.92,1,'2022-04-21 22:27:51.396127','2022-04-23 11:12:40.313939','https://res.cloudinary.com/de7jivcbv/image/upload/v1650641475/Data%20Fake/Glenfiddich-12-nam-01_f6naci.png'),(3,'Chivas 21 The Blended Grain',12,176,1,'2022-04-16 11:18:53.554417','2022-04-23 11:12:40.335039','https://res.cloudinary.com/de7jivcbv/image/upload/v1650641474/Data%20Fake/chivas-21-nam-the-blended-grain-2761_xisfan.jpg'),(4,'Absolut Vodka 1L',50,17.6,3,'2022-04-16 11:18:53.554804','2022-04-23 11:12:40.335889','https://res.cloudinary.com/de7jivcbv/image/upload/v1650641450/Data%20Fake/absolut-vodka-21_pmzamw.jpg'),(5,'Kính gọng',100,8,11,'2022-04-16 11:18:53.555185','2022-04-21 22:43:49.868376','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098067/Data%20Fake/photo-1620987278429-ab178d6eb547_c5xj6a.jpg'),(11,'Rượu gạo',54,4,10,'2022-04-16 11:18:53.557793','2022-04-23 11:12:40.394203','https://res.cloudinary.com/de7jivcbv/image/upload/v1650687076/Data%20Fake/R%C6%B0%E1%BB%A3u-G%E1%BA%A1o-Draft-Makkoli-2_l7gt6i.png'),(12,'Rượu táo mèo',4583,3,10,'2022-04-16 11:18:53.558204','2022-04-23 11:12:40.394731','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649535/Data%20Fake/ruou-ngam-sandinh-taomeo_nfsjlu.png'),(13,'Rượu ngô',5345,57,10,'2022-04-16 11:18:53.558749','2022-04-23 11:12:40.395563','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649535/Data%20Fake/ruou-ngo-bac-ha_vgcgjw.jpg'),(14,'Rượu nếp',729,443,10,'2022-04-16 11:18:53.559206','2022-04-23 11:12:40.396281','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649535/Data%20Fake/ruou-vodka-nep-moi-500-ml_qdkzzc.jpg'),(16,'Rượu mận',8923,472,10,'2022-04-16 11:18:53.560176','2022-04-23 11:12:40.396823','https://res.cloudinary.com/de7jivcbv/image/upload/v1650687144/Data%20Fake/download_egu3e0.jpg'),(36,'Nước hoa',342,8999,14,'2022-04-16 21:49:07.819689','2022-04-21 22:43:49.873403','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098064/Data%20Fake/images_tcddsr.jpg'),(37,'Đồng hồ đeo tay',30,3999,12,'2022-04-16 21:49:07.820183','2022-04-21 22:43:49.874125','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098064/Data%20Fake/images_1_mapap9.jpg'),(38,'Quần hoa',43,81,11,'2022-04-16 21:49:07.822228','2022-04-21 22:43:49.874805','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098067/Data%20Fake/photo-1624247371333-5a60f7bf1d02_nzleat.jpg'),(39,'Quần ren',18,9999,11,'2022-04-16 21:49:07.823930','2022-04-21 22:43:49.875322','https://res.cloudinary.com/de7jivcbv/image/upload/v1650118392/Data%20Fake/93954c82c45ad5ceb67b36bc0a5203df_uot67w.jpg'),(40,'Áo NYC',10,11,11,'2022-04-16 21:49:07.824372','2022-04-21 22:43:49.875742','https://res.cloudinary.com/de7jivcbv/image/upload/v1650118633/Data%20Fake/s-l400_ikrxhd.jpg'),(41,'Bag ',32,765,11,'2022-04-16 21:49:07.824982','2022-04-21 22:43:49.876456','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098169/Data%20Fake/leather-bag-gray_d1vs1b.jpg'),(42,'Chillies',332,543,13,'2022-04-16 21:49:07.825710','2022-04-21 22:43:49.877214','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098067/Data%20Fake/photo-1599987141071-f5810d32e21a_ux3zxn.jpg'),(43,'Bottle',123,421,15,'2022-04-16 21:49:07.826266','2022-04-21 22:43:49.878066','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098067/Data%20Fake/product-photo-water-bottle-hero_oawbsy.png'),(44,'Leafs',9999,1,15,'2022-04-16 21:49:07.826692','2022-04-21 22:43:49.879327','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098067/Data%20Fake/photo-1624247371333-5a60f7bf1d02_nzleat.jpg'),(45,'Cauliflower',324,64,13,'2022-04-16 21:49:07.827155','2022-04-21 22:43:49.880111','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098067/Data%20Fake/photo-1615485291234-9d694218aeb3_c9nhu3.jpg'),(46,'Candle',2445,32,15,'2022-04-16 21:49:07.828588','2022-04-21 22:43:49.880793','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098067/Data%20Fake/photo-1620987278429-ab178d6eb547_c5xj6a.jpg'),(47,'Pear',352,63,13,'2022-04-16 21:49:07.829181','2022-04-21 22:43:49.881537','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098067/Data%20Fake/photo-1615484477778-ca3b77940c25_bldbpq.jpg'),(48,'White Camera',13,4244,12,'2022-04-16 21:49:07.829648','2022-04-21 22:43:49.882216','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098067/Data%20Fake/photo-1605722625766-a4c989c747a4_bg4a1v.jpg'),(49,'Black Camera',9999,2947,12,'2022-04-16 21:49:07.830124','2022-04-21 22:43:49.882785','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098067/Data%20Fake/pexels-math-90946_rk2q1f.jpg'),(50,'Controller',423,5244,12,'2022-04-16 21:49:07.830541','2022-04-21 22:43:49.883460','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098067/Data%20Fake/photo-1600080972464-8e5f35f63d08_ndwtyo.jpg'),(51,'Smart Watch',34,8923,12,'2022-04-16 21:49:07.830973','2022-04-21 22:43:49.884149','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098066/Data%20Fake/photo-1546868871-7041f2a55e12_pa6ebw.jpg'),(52,'Kumquat',1245,45,15,'2022-04-16 21:49:07.831412','2022-04-21 22:43:49.884863','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098066/Data%20Fake/photo-1557800636-894a64c1696f_h96wgh.jpg'),(53,'Baseball',523,31,15,'2022-04-16 21:49:07.831892','2022-04-21 22:43:49.885638','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098066/Data%20Fake/photo-1554591203-3c8b71297add_raoop2.jpg'),(54,'Compact Cassette',45,223,15,'2022-04-16 21:49:07.832255','2022-04-21 22:43:49.887215','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098066/Data%20Fake/photo-1565656898731-61d5df85f9a7_mtwdp4.jpg'),(55,'Game Stick',424,323,12,'2022-04-16 21:49:07.832749','2022-04-21 22:43:49.888049','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098066/Data%20Fake/photo-1531525645387-7f14be1bdbbd_c0mqbe.jpg'),(56,'Vans Old Skool',345,231,11,'2022-04-16 21:49:07.834508','2022-04-21 22:43:49.888535','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098066/Data%20Fake/photo-1525966222134-fcfa99b8ae77_kdhm9a.jpg'),(57,'Cherry',4342,12,13,'2022-04-16 21:49:07.835006','2022-04-21 22:43:49.889128','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098066/Data%20Fake/photo-1528821128474-27f963b062bf_pruosc.jpg'),(58,'Smarter Watch',43,3234,12,'2022-04-16 21:49:07.835482','2022-04-21 22:43:49.889765','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098066/Data%20Fake/photo-1523275335684-37898b6baf30_go55kk.jpg'),(59,'Old Camera',23,445,12,'2022-04-16 21:49:07.835915','2022-04-21 22:43:49.890386','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098066/Data%20Fake/photo-1521499892833-773a6c6fd0b8_vxu2fk.jpg'),(60,'Avocado',435,22,13,'2022-04-16 21:49:07.836320','2022-04-21 22:43:49.891022','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098065/Data%20Fake/photo-1523049673857-eb18f1d7b578_msna2m.jpg'),(62,'Pin Kodak',234,64,15,'2022-04-16 21:49:07.837363','2022-04-21 22:43:49.891508','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098065/Data%20Fake/photo-1501951653466-8df816debe46_eaiiay.jpg'),(63,'UltraBoost Adidas',424,345,11,'2022-04-16 21:49:07.839131','2022-04-21 22:43:49.892235','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098065/Data%20Fake/photo-1491553895911-0055eca6402d_eotjhi.jpg'),(64,'A Tree!',1,1111,15,'2022-04-16 21:49:07.839592','2022-04-21 22:43:49.892841','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098065/Data%20Fake/photo-1467043198406-dc953a3defa0_kxpb0p.jpg'),(65,'OFF Kids',553,654,15,'2022-04-16 21:49:07.840076','2022-04-21 22:43:49.893468','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098065/Data%20Fake/OFF_Kids_Product_Collections_Large_2X_qknb02.jpg'),(66,'Luxury Headphone',45,423,12,'2022-04-16 21:49:07.840558','2022-04-21 22:43:49.894116','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098065/Data%20Fake/5_d4ykii.jpg'),(67,'Luxury Pen',54,657,11,'2022-04-16 21:49:07.840965','2022-04-21 22:43:49.894727','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098065/Data%20Fake/photo-1448471237638-f8ccc7d5ac9d_bghkfu.jpg'),(68,'Luxury Headphone but 2 color background',23,664,12,'2022-04-16 21:49:07.841287','2022-04-21 22:43:49.895420','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098064/Data%20Fake/istockphoto-1072492806-170667a_w34urz.jpg'),(69,'Brown Body mist',435,2325,14,'2022-04-16 21:49:07.841630','2022-04-21 22:43:49.896122','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098064/Data%20Fake/5def862ab67e21987db97ca9_Featured_October_1_tkezrr.png'),(70,'Typewriter',32,8768,15,'2022-04-16 21:49:07.842082','2022-04-21 22:43:49.896662','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098064/Data%20Fake/LJIZlzHgQ7WPSh5KVTCB_Typewriter_ekgljl.jpg'),(71,'Yellow Body mist',54,2452,14,'2022-04-16 21:49:07.842586','2022-04-21 22:43:49.897241','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098064/Data%20Fake/images_tcddsr.jpg'),(72,'Watch',43,3524,11,'2022-04-16 21:49:07.843042','2022-04-21 22:43:49.897839','https://res.cloudinary.com/de7jivcbv/image/upload/v1650098064/Data%20Fake/images_1_mapap9.jpg'),(80,'Hennessy VSOP Limited Refik Anadol',42,82,2,'2022-04-22 22:11:34.145540','2022-04-23 11:12:40.336512','https://res.cloudinary.com/de7jivcbv/image/upload/v1650641474/Data%20Fake/hennessy-vsop-limited-refik-anadol-2449_gkg2fw.jpg'),(81,'Hennessy X.O',12,220,2,'2022-04-23 00:41:52.434721','2022-04-23 11:12:40.337176','https://res.cloudinary.com/de7jivcbv/image/upload/v1650641475/Data%20Fake/hennessy-xo-limited-frank-gehry-2469_udjvyn.jpg'),(82,'Hennessy X.O ICE Discovery',10,242,2,'2022-04-23 00:41:52.436782','2022-04-23 11:12:40.340506','https://res.cloudinary.com/de7jivcbv/image/upload/v1650641474/Data%20Fake/hennessy-xo-ice-discovery-949_bts1tz.jpg'),(83,'Remy Martin Club',21,77,2,'2022-04-23 00:41:52.437180','2022-04-23 11:12:40.341084','https://res.cloudinary.com/de7jivcbv/image/upload/v1650685153/Data%20Fake/remy-martin-club-hop-qua-tet-2020-1292_i1crkk.jpg'),(84,'Courvoísier XO',32,141,2,'2022-04-23 00:41:52.437573','2022-04-23 11:12:40.341707','https://res.cloudinary.com/de7jivcbv/image/upload/v1650685345/Data%20Fake/courvoisier-xo-215_rlgp76.jpg'),(85,'Bache Cognac XO',22,141,2,'2022-04-23 00:41:52.438008','2022-04-23 11:12:40.342340','https://res.cloudinary.com/de7jivcbv/image/upload/v1650641473/Data%20Fake/bache-cognac-xo-1320_ndefyy.jpg'),(86,'Ballon d\'Or X.O',4,19,2,'2022-04-23 00:41:52.441026','2022-04-23 11:12:40.342890','https://res.cloudinary.com/de7jivcbv/image/upload/v1650641474/Data%20Fake/ballon-dor-xo-2400_t5cqz9.jpg'),(87,'Baron Otard VSOP',30,58,2,'2022-04-23 00:41:52.441508','2022-04-23 11:12:40.343491','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649535/Data%20Fake/baron-otard-vsop-68_xkihaa.jpg'),(88,'Baron Otard XO',12,140.8,2,'2022-04-23 00:41:52.441943','2022-04-23 11:12:40.344100','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649535/Data%20Fake/baron-otard-xo-70_alwkdy.jpg'),(89,'Berville XO',38,22,2,'2022-04-23 00:41:52.442336','2022-04-23 11:12:40.346048','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649535/Data%20Fake/berville-xo-88_zigstx.jpg'),(90,'Mortlach 12',32,83.6,1,'2022-04-23 00:41:52.442672','2022-04-23 11:12:40.346675','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649535/Data%20Fake/mortlach-12-nam-hop-qua-tet-2021-2307_pyg6zj.jpg'),(91,'Jonnie Walker Black Label Pocket',51,10,1,'2022-04-23 00:41:52.443061','2022-04-23 11:12:40.347371','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649534/Data%20Fake/johnnie-walker-black-label-pocket-1814_jilfuo.jpg'),(92,'Jonnie Walker Red Label Pocket',81,6.16,1,'2022-04-23 00:41:52.443467','2022-04-23 11:12:40.351696','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649534/Data%20Fake/johnnie-walker-red-label-pocket-1815_vwloa6.jpg'),(93,'Jonnie Walker A Song Of Fire',12,44,1,'2022-04-23 00:41:52.443903','2022-04-23 11:12:40.352629','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649535/Data%20Fake/johnnie-walker-a-song-of-fire-rong-1958_yia8yt.jpg'),(94,'Jonnie Walker A Song Of Ice',11,44,1,'2022-04-23 00:41:52.444365','2022-04-23 11:12:40.353227','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649534/Data%20Fake/johnnie-walker-a-song-of-ice-soi-1663_bmlqjk.jpg'),(95,'Chivas 15',32,50.6,1,'2022-04-23 00:41:52.444905','2022-04-23 11:12:40.353857','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649534/Data%20Fake/chivas-15-nam-hop-qua-tet-2020-935_cumq9v.jpg'),(96,'Chivas 18 Ultimate Cask Collection',31,80,1,'2022-04-23 00:41:52.446450','2022-04-23 11:12:40.354381','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649534/Data%20Fake/chivas-18-ultimate-cask-collection-american-oak-1287_xoyud5.jpg'),(97,'Chivas 21 Royal Salute',12,132,1,'2022-04-23 00:41:52.447170','2022-04-23 11:12:40.354939','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649534/Data%20Fake/chivas-21-nam-hop-qua-tet-2020-938_avq56s.jpg'),(98,'Chivas 25 Regal',14,260,1,'2022-04-23 00:41:52.447667','2022-04-23 11:12:40.355455','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649534/Data%20Fake/chivas-regal-25-nam-182_edf61b.jpg'),(99,'Chivas Regal Mizunara (Chivas Nhật)',12,50.6,1,'2022-04-23 00:41:52.448218','2022-04-23 11:12:40.356012','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649534/Data%20Fake/chivas-regal-mizunara-chivas-nhat-183_m2yrai.jpg'),(100,'Ballantine\'s Finest',34,20.68,1,'2022-04-23 00:41:52.448737','2022-04-23 11:12:40.356570','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649534/Data%20Fake/Ballantine_s_Finest_Tet_2020_rziuay.jpg'),(101,'Chivas 21 The Polo Edition (2017)',7,160.6,1,'2022-04-23 00:41:52.449218','2022-04-23 11:12:40.357114','https://res.cloudinary.com/de7jivcbv/image/upload/v1650685888/Data%20Fake/chivas-21-nam-the-polo-edition-2017-2149_nxw6wv.jpg'),(102,'Glenlivet 18',32,137,1,'2022-04-23 00:41:52.449642','2022-04-23 11:12:40.357660','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649533/Data%20Fake/Glenlivet_18_Tet_2020_k4pudm.jpg'),(103,'Macallan Folio 6',12,225,1,'2022-04-23 00:41:52.450991','2022-04-23 11:12:40.358247','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649534/Data%20Fake/e7de9eb51f7e43ac9ba361369830eb67_s2jg0y.jpg'),(104,'Smirnoff Vodka Red',42,13.2,3,'2022-04-23 00:41:52.451531','2022-04-23 11:12:40.358836','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649533/Data%20Fake/smirnoff-vodka-red-625_ljv3oz.jpg'),(105,'Danzka',31,15.4,3,'2022-04-23 00:41:52.451887','2022-04-23 11:12:40.359388','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649533/Data%20Fake/danzka-1l-226_im4rvg.jpg'),(106,'Ciroc Vodka',53,15.4,3,'2022-04-23 00:41:52.452369','2022-04-23 11:12:40.359953','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649533/Data%20Fake/ciroc-vodka-206_xxecft.jpg'),(107,'Grey goose vodka',21,33,3,'2022-04-23 00:41:52.452815','2022-04-23 11:12:40.360689','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649533/Data%20Fake/grey-goose-vodka-304_pbmtg1.jpg'),(108,'Standard Vodka',53,15.4,3,'2022-04-23 00:41:52.453228','2022-04-23 11:12:40.361371','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649533/Data%20Fake/standard-vodka-648_v4fnjw.jpg'),(109,'Belvedere Vodka',23,44,3,'2022-04-23 00:41:52.453696','2022-04-23 11:12:40.361958','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649532/Data%20Fake/belvedere-vodka-78_dknwwh.jpg'),(110,'Absolut Elyx',12,66,3,'2022-04-23 00:41:52.454990','2022-04-23 11:12:40.362586','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649532/Data%20Fake/absolut-elyx-11_rppp6b.jpg'),(111,'Absolut Vodka Apeach',18,17.6,3,'2022-04-23 00:41:52.455500','2022-04-23 11:12:40.363172','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649532/Data%20Fake/absolut-vodka-apeach-dao-4_j2qygq.jpg'),(112,'Absolut Electrik',12,20,3,'2022-04-23 00:41:52.455924','2022-04-23 11:12:40.363742','https://res.cloudinary.com/de7jivcbv/image/upload/v1650686091/Data%20Fake/absolut-electrik-mau-xanh-mau-bac-6_igh1qi.jpg'),(113,'Absolut Vodka Citron',18,17.6,3,'2022-04-23 00:41:52.456325','2022-04-23 11:12:40.364334','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649532/Data%20Fake/absolut-citron-vodka-chanh-5_ghnicn.jpg'),(114,'Skyy Vodka',12,13.2,3,'2022-04-23 00:41:52.457471','2022-04-23 11:12:40.364908','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649532/Data%20Fake/skyy-vodka-611_qsf1br.jpg'),(115,'Jagermeister',21,18,4,'2022-04-23 00:41:52.457913','2022-04-23 11:12:40.365461','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649532/Data%20Fake/jagermeister-363_dcqx0u.jpg'),(116,'Absinthe Absente 55',54,62,4,'2022-04-23 00:41:52.458324','2022-04-23 11:12:40.366032','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649532/Data%20Fake/absinthe-absente-55-2432_haegsd.jpg'),(117,'Angostura Aromatic Bitters',21,20,4,'2022-04-23 00:41:52.458736','2022-04-23 11:12:40.366689','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649532/Data%20Fake/angostura-aromatic-bitters-966_t2pwta.jpg'),(118,'Aperol',34,22,4,'2022-04-23 00:41:52.459103','2022-04-23 11:12:40.367192','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649532/Data%20Fake/aperol-30_f76zmh.jpg'),(119,'Appleton Estate 21 Jamaica Rum',12,50,4,'2022-04-23 00:41:52.459506','2022-04-23 11:12:40.367765','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649532/Data%20Fake/appleton-estate-21-jamaica-rum-1827_jjboq8.jpg'),(120,'Appleton Jamaica Special',73,29,4,'2022-04-23 00:41:52.459951','2022-04-23 11:12:40.368443','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649532/Data%20Fake/appleton-jamaica-special-1929_mzkag1.jpg'),(121,'Bacardi 8',12,47,4,'2022-04-23 00:41:52.460319','2022-04-23 11:12:40.369061','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649531/Data%20Fake/bacardi-8-nam-969_vt4q9t.jpg'),(122,'Bacardi Dark Rum',43,28.9,4,'2022-04-23 00:41:52.461063','2022-04-23 11:12:40.369668','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649531/Data%20Fake/bacardi-superior-carta-negra-dark-rum-33_oe89az.jpg'),(123,'Bacardi Gold Rum',31,24.6,4,'2022-04-23 00:41:52.461474','2022-04-23 11:12:40.370279','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649531/Data%20Fake/bacardi-gold-rum-32_oombsx.jpg'),(124,'Bacardi Limon',32,20,4,'2022-04-23 00:41:52.461884','2022-04-23 11:12:40.370730','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649531/Data%20Fake/bacardi-limon-1920_hixfly.jpg'),(125,'Baileys Original',23,18.2,4,'2022-04-23 00:41:52.462279','2022-04-23 11:12:40.371170','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649531/Data%20Fake/baileys-original-37_cqxc6g.jpg'),(126,'Bardinet Cherry Brandy',32,19,4,'2022-04-23 00:41:52.462694','2022-04-23 11:12:40.371581','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649531/Data%20Fake/bardinet-cherry-brandy-61_jg7bgp.jpg'),(127,'Bardinet Coconut',14,19,4,'2022-04-23 00:41:52.463028','2022-04-23 11:12:40.372087','https://res.cloudinary.com/de7jivcbv/image/upload/v1650686368/Data%20Fake/bardinet-coconut-62_ifk15p.jpg'),(128,'Becherovka',42,28.6,4,'2022-04-23 00:41:52.463431','2022-04-23 11:12:40.372706','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649530/Data%20Fake/becherovka-2755_up21fd.jpg'),(129,'Beefeater London Dry Gin',42,22,4,'2022-04-23 00:41:52.463811','2022-04-23 11:12:40.373277','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649530/Data%20Fake/beefeater-london-dry-gin-975_lhgu3k.jpg'),(130,'Benedictine Dom',23,32,4,'2022-04-23 00:41:52.464151','2022-04-23 11:12:40.373869','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649530/Data%20Fake/benedictine-dom-84_snqwnf.jpg'),(131,'Bickens London Dry Gin',53,26.52,4,'2022-04-23 00:41:52.464534','2022-04-23 11:12:40.374411','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649530/Data%20Fake/bickens-london-dry-gin-1372_ebr95d.jpg'),(132,'Bols Amaretto',12,15.4,4,'2022-04-23 00:41:52.464943','2022-04-23 11:12:40.374972','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649530/Data%20Fake/bols-amaretto-90_bv9bcf.jpg'),(133,'Bols Black Berry',32,15.4,4,'2022-04-23 00:41:52.465348','2022-04-23 11:12:40.375579','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649530/Data%20Fake/bols-black-berry-2118_xpt4a0.jpg'),(134,'Bols Coconut',32,15.4,4,'2022-04-23 00:41:52.465798','2022-04-23 11:12:40.376164','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649530/Data%20Fake/bols-coconut-2120_frskky.jpg'),(135,'Bols Dry Orange',32,15.4,4,'2022-04-23 00:41:52.466179','2022-04-23 11:12:40.376763','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649539/Data%20Fake/bols-dry-orange-97_kbs58o.jpg'),(136,'Bols Maracuja Passion Fruit',24,15.4,4,'2022-04-23 00:41:52.466524','2022-04-23 11:12:40.377337','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649539/Data%20Fake/bols-passion-fruit-984_nieini.jpg'),(137,'Chambord',31,44,4,'2022-04-23 00:41:52.466875','2022-04-23 11:12:40.377910','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649538/Data%20Fake/chambord-149_fnbprz.jpg'),(138,'Choya Organic',34,35.2,4,'2022-04-23 00:41:52.467388','2022-04-23 11:12:40.378529','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649538/Data%20Fake/choya-organic-680_apt3ut.jpg'),(139,'Dead Man\'s Fingers Spiced',12,39.6,4,'2022-04-23 00:41:52.467766','2022-04-23 11:12:40.379150','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649539/Data%20Fake/dead-mans-fingers-spiced-2171_gj4lym.jpg'),(140,'Disaronno since 1525',45,32.5,4,'2022-04-23 00:41:52.468165','2022-04-23 11:12:40.379786','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649538/Data%20Fake/disaronno-since-1525-238_eaj4zm.jpg'),(141,'Diplomatico Planas',54,83.6,4,'2022-04-23 00:41:52.468536','2022-04-23 11:12:40.380372','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649538/Data%20Fake/diplomatico-planas-2331_lky23m.jpg'),(142,'Don Julio 1942 Anejo',12,255.2,4,'2022-04-23 00:41:52.469617','2022-04-23 11:12:40.380878','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649538/Data%20Fake/don-julio-1942-anejo-2501_nmiawn.jpg'),(143,'Dolin Vermouth de Chambery Rouge',56,24.64,4,'2022-04-23 00:41:52.469966','2022-04-23 11:12:40.381373','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649538/Data%20Fake/dolin-vermouth-de-chambery-rouge-2191_vpzfhi.jpg'),(144,'Espolon Tequila Reposado',21,59.4,4,'2022-04-23 00:41:52.470365','2022-04-23 11:12:40.381818','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649537/Data%20Fake/espolon-tequila-reposado-249_tpeuba.jpg'),(145,'Flor de Cana 25',11,193.6,4,'2022-04-23 00:41:52.470772','2022-04-23 11:12:40.382314','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649537/Data%20Fake/f3495bb01c814fe3a1461231cea522fd_yx71jk.jpg'),(146,'Four Pillars Bloody Shiraz Gin',21,55,4,'2022-04-23 00:41:52.471302','2022-04-23 11:12:40.382832','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649537/Data%20Fake/f3495bb01c814fe3a1461231cea522fd_yx71jk.jpg'),(147,'Galliano L\'Autentico Liqueur',31,48.4,4,'2022-04-23 00:41:52.471698','2022-04-23 11:12:40.383339','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649537/Data%20Fake/638ddb8b5fa6414fafd589a4d429d9e4_kubuwd.jpg'),(148,'Gordon\'s Gin',43,5.28,4,'2022-04-23 00:41:52.472913','2022-04-23 11:12:40.383830','https://res.cloudinary.com/de7jivcbv/image/upload/v1650686681/Data%20Fake/gordons-gin-295_swtozd.jpg'),(149,'Vang M Merlot Salento',12,37.4,5,'2022-04-23 00:41:52.473348','2022-04-23 11:12:40.384369','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649537/Data%20Fake/ea02acbec66346358849d903c0ad4651_bds1es.jpg'),(150,'Jacob\'s Creek Double Barrel Shiraz',12,35.2,5,'2022-04-23 00:41:52.473717','2022-04-23 11:12:40.384896','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649537/Data%20Fake/jacobs-creek-double-barrel-shiraz-2214_xf7hfx.jpg'),(151,'Alexis Lichine Merlot',15,15.4,5,'2022-04-23 00:41:52.474032','2022-04-23 11:12:40.385432','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649537/Data%20Fake/alexis-lichine-merlot-27_vjzlpl.jpg'),(152,'Baglietti No.10',12,44,5,'2022-04-23 00:41:52.474411','2022-04-23 11:12:40.385917','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649537/Data%20Fake/baglietti-no10-2420_kqpddt.jpg'),(153,'Bottega Gold',32,43.56,5,'2022-04-23 00:41:52.474782','2022-04-23 11:12:40.386445','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649537/Data%20Fake/bottega-gold-1829_gofvsx.jpg'),(154,'Bottega Moscato',12,43.56,5,'2022-04-23 00:41:52.475128','2022-04-23 11:12:40.387067','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649537/Data%20Fake/bottega-moscato-hoa-hong-1830_hnzsbn.jpg'),(155,'Cafe de paris Lychee',8,20,5,'2022-04-23 00:41:52.475551','2022-04-23 11:12:40.387594','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649536/Data%20Fake/cafe-de-paris-lychee-mui-vai-127_ieukiy.jpg'),(156,'Campo Viejo Gran Reserva Red',2,33,5,'2022-04-23 00:41:52.475905','2022-04-23 11:12:40.388085','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649536/Data%20Fake/campo-viejo-gran-reserva-red-133_tbbw2h.jpg'),(157,'Casa merlot',4,15.4,5,'2022-04-23 00:41:52.476332','2022-04-23 11:12:40.388618','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649536/Data%20Fake/casa-merlot-148_gtn07h.jpg'),(158,'Champagne Armand De Brignac Gold Brut',3,404.8,5,'2022-04-23 00:41:52.476703','2022-04-23 11:12:40.389071','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649536/Data%20Fake/champagne-armand-de-brignac-gold-brut-ach-bich-1864_umjp0t.jpg'),(159,'Champagne Armand De Brignac Rose',2,422.4,5,'2022-04-23 00:41:52.477121','2022-04-23 11:12:40.389621','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649536/Data%20Fake/champagne-armand-de-brignac-rose-ach-bich-hong-2758_fmnnmi.jpg'),(160,'Champagne G.H Mumm',3,167.2,5,'2022-04-23 00:41:52.477516','2022-04-23 11:12:40.390088','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649536/Data%20Fake/champagne-gh-mumm-150_tmqtdl.jpg'),(161,'Monin Bubble Gum',5,11,5,'2022-04-23 00:41:52.477891','2022-04-23 11:12:40.390591','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649536/Data%20Fake/monin-bubble-gum-2561_kuf37x.jpg'),(162,'Monin Coconut',8,11,5,'2022-04-23 00:41:52.478362','2022-04-23 11:12:40.391069','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649536/Data%20Fake/monin-coconut-dua-2133_h4fsfd.jpg'),(163,'Monin Kiwi',8,11,5,'2022-04-23 00:41:52.478714','2022-04-23 11:12:40.391608','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649536/Data%20Fake/monin-kiwi-2564_fjysvc.jpg'),(164,'Sake Dassai 23',5,114.4,5,'2022-04-23 00:41:52.479129','2022-04-23 11:12:40.392110','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649536/Data%20Fake/Sake-Dassai-23-2_g4chhx.png'),(165,'Santa Helena Parras Viejas',7,74.8,5,'2022-04-23 00:41:52.479514','2022-04-23 11:12:40.392632','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649536/Data%20Fake/santa-helena-parras-viejas-589_jkafug.jpg'),(166,'Soju Jinro Green Grap',13,3,5,'2022-04-23 00:41:52.479831','2022-04-23 11:12:40.393115','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649536/Data%20Fake/soju-jinro-green-grape-nho-630_g2vjgo.jpg'),(167,'NTC Patritti Apple Juice',23,6,5,'2022-04-23 00:41:52.480141','2022-04-23 11:12:40.393677','https://res.cloudinary.com/de7jivcbv/image/upload/v1650649535/Data%20Fake/ntc-patritti-apple-juice-tao-509_s8ahjb.jpg'),(168,'Bia 333 330ml',143,0.4,9,'2022-04-23 00:41:52.480500','2022-04-23 11:18:46.857156','https://res.cloudinary.com/de7jivcbv/image/upload/v1650687463/Data%20Fake/333_p4emni.jpg'),(169,'Heineken 330ml',132,1,9,'2022-04-23 00:41:52.480916','2022-04-23 11:18:46.859023','https://res.cloudinary.com/de7jivcbv/image/upload/v1650687463/Data%20Fake/heineken_xug0rc.jpg'),(170,'Budweiser 330ml',123,0.8,9,'2022-04-23 00:41:52.481363','2022-04-23 11:18:46.859802','https://res.cloudinary.com/de7jivcbv/image/upload/v1650687463/Data%20Fake/BIA-BUDWEISER-330ML_wmawkq.jpg'),(171,'Bia Sài Gon 330ml',112,0.4,9,'2022-04-23 00:41:52.481722','2022-04-23 11:18:46.860482','https://res.cloudinary.com/de7jivcbv/image/upload/v1650687463/Data%20Fake/SaiGon_ubzm1j.jpg'),(172,'Bia Tiger Crystal 330ml',153,0.6,9,'2022-04-23 00:41:52.482465','2022-04-23 11:18:46.861089','https://res.cloudinary.com/de7jivcbv/image/upload/v1650687463/Data%20Fake/tigercrystal_keimph.jpg'),(173,'Bia Huda 330ml',123,0.4,9,'2022-04-23 00:41:52.482834','2022-04-23 11:18:46.861762','https://res.cloudinary.com/de7jivcbv/image/upload/v1650687464/Data%20Fake/Huda_vrleiy.jpg'),(174,'Bia Hà Nội 330ml',423,0.4,9,'2022-04-23 00:41:52.483234','2022-04-23 11:18:46.862375','https://res.cloudinary.com/de7jivcbv/image/upload/v1650687464/Data%20Fake/bia-ha-noi-premium-lon-330ml_xtouci.jpg'),(175,'Bia Corona 355ml',141,2,9,'2022-04-23 00:41:52.483609','2022-04-23 11:18:46.863045','https://res.cloudinary.com/de7jivcbv/image/upload/v1650687464/Data%20Fake/Bia-corona-extra_prlcy2.png');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `profile` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `accountId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `REL_36059b560e94dbaa061527a85c` (`accountId`),
  CONSTRAINT `FK_36059b560e94dbaa061527a85ce` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES (1,'nDuy',NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-24 17:43:50
