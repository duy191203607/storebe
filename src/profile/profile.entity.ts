import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Account } from "../account/account.entity";

@Entity()
export class Profile {
  @PrimaryGeneratedColumn({ type: "int" })
  id: number;
  @Column({ nullable: true, length: 100, type: "varchar", default: null })
  name: string;
  @Column({ nullable: true, length: 60, type: "varchar", default: null })
  phone: string;
  @Column({ nullable: true, length: 50, type: "varchar", default: null })
  address: string;
  @Column({ nullable: true, length: 50, type: "varchar", default: null })
  image: string;
  @OneToOne(() => Account, account => account.id)
  @JoinColumn()
  account: Account;
}