import { forwardRef, Module } from "@nestjs/common";
import { OrderController } from './order.controller';
import { OrderService } from './order.service';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Order, OrderProduct } from "./order.entity";
import { AccountModule } from "../account/account.module";
import { ProductModule } from "../product/product.module";

@Module({
  imports: [
    TypeOrmModule.forFeature([Order, OrderProduct]),
    forwardRef(() => AccountModule),
    ProductModule
  ],
  controllers: [OrderController],
  providers: [OrderService],
  exports: [TypeOrmModule]
})
export class OrderModule {}
