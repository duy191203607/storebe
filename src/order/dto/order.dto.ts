import {
  IsArray,
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  MaxLength
} from "class-validator";
interface IProductOrder{
  id: number,
  quantity: number
}
export class CreateOrderDto {
  @IsNumber()
  @IsNotEmpty()
  price: number;
  @IsArray()
  @IsNotEmpty()
  product: IProductOrder[];
  @IsNotEmpty()
  @IsString()
  @IsNotEmpty()
  @MaxLength(200)
  name: string;
  @IsString()
  @IsNotEmpty()
  phone: string;
  @IsEmail()
  @IsOptional()
  email?: string;
  @IsString()
  @IsNotEmpty()
  @MaxLength(200)
  address: string;
}