import { Body, Controller, Get, HttpException, HttpStatus, Param, ParseIntPipe, Post, Req, Res } from "@nestjs/common";
import { OrderService } from "./order.service";
import { CreateOrderDto } from "./dto/order.dto";
import { Response, Request } from "express";
import { JwtService } from "@nestjs/jwt";

@Controller("order")
export class OrderController {
  constructor(private readonly orderService: OrderService, private readonly jwtService: JwtService) {
  }


  @Post("/create")
  async createOrder(@Body() createOrderDto: CreateOrderDto, @Res() res: Response, @Req() req: Request) {
    try {
      console.log(createOrderDto);
      const tokenDecode: any = this.jwtService.decode(req.cookies["access_token"]);
      await this.orderService.createNewOrder(createOrderDto, tokenDecode.id);
      res.status(200).json({ success: true, message: "Order has been created" });
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }

  @Get(":id")
  async getOrderById(@Param("id", ParseIntPipe) id: number) {
    try {
      return await this.orderService.getById(id);
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }
}
