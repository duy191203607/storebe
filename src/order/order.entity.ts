import {
  Column,
  CreateDateColumn,
  Entity, JoinColumn, JoinTable,
  ManyToMany,
  ManyToOne, OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import { Account } from "../account/account.entity";
import { Product } from "../product/product.entity";

@Entity()
export class Order {
  @PrimaryGeneratedColumn({ type: "smallint" })
  id: number;
  @Column({ type: "float" })
  price: number;
  @Column({ type: "varchar", nullable: false })
  name: string;
  @Column({ type: "varchar", nullable: false })
  phone: string;
  @Column({ type: "varchar", nullable: true })
  email: string;
  @Column({ type: "varchar", nullable: false, length: 200 })
  address: string;
  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;
  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;
  @ManyToOne(() => Account)
  account: Account;
  @OneToMany(() => OrderProduct, (orderProduct) => orderProduct.order)
  products: OrderProduct[]
}

@Entity("order_product")
export class OrderProduct {
  @PrimaryGeneratedColumn("uuid")
  id: string;
  @Column({ name: "quantity",type: "smallint", nullable: false })
  quantity: number;
  @ManyToOne(() => Product, (product) => product.productToOrder)
  @JoinColumn({name: "productId"})
  product: Product;
  @ManyToOne(() => Order, (order) => order.products)
  @JoinColumn({name: "orderId"})
  order: Order
}