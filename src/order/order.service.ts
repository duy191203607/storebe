import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Order, OrderProduct } from "./order.entity";
import { Repository } from "typeorm";
import { CreateOrderDto } from "./dto/order.dto";
import { Account } from "../account/account.entity";
import { Product } from "../product/product.entity";

@Injectable()
export class OrderService {
  constructor(@InjectRepository(Order) private readonly orderRepo: Repository<Order>,
              @InjectRepository(Account) private readonly accountRepo: Repository<Account>,
              @InjectRepository(Product) private readonly productRepo: Repository<Product>,
              @InjectRepository(OrderProduct) private readonly orderProductRepository: Repository<OrderProduct>) {
  }

  async getById(id: number) {
    return await this.orderRepo.findOne(id, { relations: ["product"] });
  }

  async createNewOrder({ price, product, name, email, phone, address }: CreateOrderDto, accountId: number) {
    const accountCreateOrder = await this.accountRepo.findOne(accountId);
    if (!accountCreateOrder) {
      throw new HttpException("Not found user!", HttpStatus.BAD_REQUEST);
    }
    const newOrder = this.orderRepo.create({
      price,
      account: accountCreateOrder,
      name,
      phone,
      address,
      ...(email && { email })
    });
    const newOrderSaved = await this.orderRepo.save(newOrder);
    product.map(async (v) => {
      const productToOrder = await this.productRepo.findOne(v.id)
      const newOrderToProduct = this.orderProductRepository.create({
        quantity: v.quantity,
        order: newOrderSaved,
        product: productToOrder
      });
      await this.orderProductRepository.save(newOrderToProduct);
    });
    return newOrderSaved
  }
}
