import { CacheModule, Global, Module } from "@nestjs/common";
import { AppService } from "./app.service";
import { AuthModule } from "./auth/auth.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AccountModule } from "./account/account.module";
import { ProfileModule } from "./profile/profile.module";
import { join } from "path";
import { ProductModule } from "./product/product.module";
import { CategoryModule } from "./category/category.module";
import { OrderModule } from "./order/order.module";
import redisStore from "cache-manager-redis-store";
import { ConfigModule, ConfigService } from "@nestjs/config";

@Module({
  providers: [AppService],
  imports: [
    AuthModule,
    AccountModule,
    ProfileModule,
    TypeOrmModule.forRoot({
      type: "mysql",
      host: process.env.MYSQL_HOST,
      port: parseInt(process.env.MYSQL_PORT),
      username: "root",
      password: "01Daongocduy",
      database: "store",
      entities: [join(__dirname, "**", "*.entity.{ts,js}")],
      synchronize: true,
      autoLoadEntities: true,
      logging: process.env.NODE_ENV === "development"
    }),
    CacheModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      isGlobal: true,
      useFactory: (configService: ConfigService) => ({
        store: redisStore,
        host: "cache_server",
        port: configService.get("REDIS_PORT"),
        ttl: 0
      })
    }),

    ProductModule,
    CategoryModule,
    OrderModule
  ]
})
export class AppModule {
}
