import {ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus} from "@nestjs/common";

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {

  catch(exception: HttpException, host: ArgumentsHost): any {
    const ctx = host.switchToHttp();
    const {message}: any = exception.getResponse();
    const response = ctx.getResponse();
    const request = ctx.getRequest();
    const status = exception instanceof HttpException? exception.getStatus(): HttpStatus.INTERNAL_SERVER_ERROR;
    const responseBody = {
      status: status,
      error: exception.message,
      message,
      timestamp: new Date().toISOString(),
      path: request.url,
    }
    response.status(status).json(responseBody)
  }
}