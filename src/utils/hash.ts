import * as crypto from "crypto";

const salt = crypto.randomBytes(16).toString("hex");
export const hashString = (hashString: string) => {
  return crypto.pbkdf2Sync(hashString, salt,
    1000, 64, `sha512`).toString(`hex`);
}
export const verifyHashString = (stringVerify: string, hashedString: string) => {
  const hash = crypto.pbkdf2Sync(stringVerify, salt,
    1000, 64, `sha512`).toString(`hex`);
  return hash === hashedString;
}