import {Controller, Get, HttpException, HttpStatus} from "@nestjs/common";
import {CategoryService} from "./category.service";

@Controller('category')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {
  }

  @Get()
  async getAll() {
    try {
      return this.categoryService.getAll();
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }
}