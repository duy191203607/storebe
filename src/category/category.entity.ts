import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Product } from "../product/product.entity";

@Entity()
export class Category {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ type: "varchar", length: 1000, nullable: false })
  name: string;
  @OneToMany(() => Product,
    (product) => product.category,
    { onDelete: "CASCADE", onUpdate: "CASCADE" })
  products: Array<Product>;
}