import { IsNotEmpty, IsNumber, IsString } from "class-validator";
import { Type } from "class-transformer";

export class TestCurrentPasswordDto{
  @IsNotEmpty()
  @IsNumber()
  @Type(() => Number)
  accountId: number
  @IsNotEmpty()
  @IsString()
  currentPassword: string
}