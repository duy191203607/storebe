import { Body, CACHE_MANAGER, Controller, HttpException, HttpStatus, Inject, Post, Req, Res } from "@nestjs/common";
import { LoginDto } from "./dto/login.dto";
import { AuthService } from "./auth.service";
import { AccessTokenPayload } from "./accessToken.payload";
import { JwtService } from "@nestjs/jwt";
import { Response, Request } from "express";
import { RegisterDto } from "./dto/register.dto";
import { Cache } from "cache-manager";
import { TestCurrentPasswordDto } from "./dto/test-current-password.dto";

@Controller("auth")
export class AuthController {
  constructor(
    private readonly authService: AuthService, private readonly jwtService: JwtService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache
  ) {
  }

  @Post("login")
  async login(@Body() loginDTO: LoginDto, @Res() res: Response) {
    try {
      const account = await this.authService.validLoginAccount(loginDTO);
      const tokenPayload: AccessTokenPayload = {
        id: account.id,
        email: account.email,
        name: account.profile.name
      };
      const accessToken = this.jwtService.sign(tokenPayload);
      const cachedSession = await this.cacheManager.get(`${account.email}_token`);
      if (cachedSession) {
        await this.cacheManager.del(`${account.email}_token`);
      }
      await this.cacheManager.set(`${account.email}_token`, accessToken, { ttl: 0 });
      res
        .status(200)
        .cookie("access_token", accessToken, {
          httpOnly: true
        })
        .json({
          message: "Login success",
          data: tokenPayload
        });
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }

  @Post("register")
  async register(@Body() registerDTO: RegisterDto, @Res() res: Response) {
    try {
      const account = await this.authService.registerUser(registerDTO);
      if (account) {
        const payloadToken: AccessTokenPayload = {
          email: account.email,
          id: account.account.id,
          name: account.name
        };
        const token = this.jwtService.sign(payloadToken);

        await this.cacheManager.set(`${account.email}_token`, token, { ttl: 0 });
        res.status(200).cookie("access_token", token).json({
          status: 200,
          data: payloadToken
        });
      }
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }

  @Post("logout")
  async logout(@Res() res: Response, @Req() req: Request) {
    try {
      const token = req.cookies["access_token"];
      const tokenDecode: any = this.jwtService.decode(token);

      if (tokenDecode) {
        await this.cacheManager.del(`${tokenDecode.email}_token`);
      }
      res.clearCookie("access_token").json({
        success: true
      });
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }

  @Post("/test-current-pass")
  async testCurrentPassword(@Body() currentPassword: TestCurrentPasswordDto, @Res() res: Response) {
    try {
      await this.authService.validCurrentPassword(currentPassword);
      res.status(200).json({
        data: true
      });
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }
}