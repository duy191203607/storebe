import { CacheModule, Global, Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Account } from "../account/account.entity";
import { AuthService } from "./auth.service";
import { PassportModule } from "@nestjs/passport";
import { JwtModule } from "@nestjs/jwt";
import { JwtConstants } from "./constants";
import { JwtStrategy } from "./jwt.strategy";
import { AuthController } from "./auth.controller";
import { AccountModule } from "../account/account.module";
import { ProfileModule } from "../profile/profile.module";

@Global()
@Module({
  imports: [
    PassportModule,
    JwtModule.register({
      secret: JwtConstants.secret
    }),
    AccountModule,
    ProfileModule
  ],
  providers: [AuthService, JwtStrategy],
  controllers: [AuthController],
  exports: [JwtModule]
})
export class AuthModule {
}
