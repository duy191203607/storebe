import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Account } from "../account/account.entity";
import { Repository } from "typeorm";
import { LoginDto } from "./dto/login.dto";
import { RegisterDto } from "./dto/register.dto";
import { Profile } from "../profile/profile.entity";
import * as bcrypt from "bcrypt";
import { TestCurrentPasswordDto } from "./dto/test-current-password.dto";

@Injectable()
export class AuthService {
  constructor(@InjectRepository(Account) private readonly accountRepository: Repository<Account>,
              @InjectRepository(Profile) private readonly profileRepository: Repository<Profile>
  ) {
  }

  async validLoginAccount(loginDTO: LoginDto) {
    const { email, password } = loginDTO;
    const user = await this.accountRepository.findOne({
      where: {
        email
      },
      relations: ["profile"]
    });
    if (!user) {
      throw new HttpException("User not found!", HttpStatus.BAD_REQUEST);
    }
    const validPassword = await bcrypt.compare(password, user.password);
    if (!validPassword) {
      throw new HttpException("Email/password do not match", HttpStatus.BAD_REQUEST);
    }
    return user;
  }

  async registerUser(registerDTO: RegisterDto) {
    const { email, password, name, phone, address } = registerDTO;
    const user = await this.accountRepository.findOne({ email });
    if (user) {
      throw new HttpException("User is already exits!", HttpStatus.BAD_REQUEST);
    }
    const hashedPassword = await bcrypt.hash(password, 10);
    const newUser = this.accountRepository.create({ ...registerDTO, password: hashedPassword });
    await this.accountRepository.save(newUser);
    const newUserProfile = this.profileRepository.create({
      account: newUser,
      ...(name && { name }),
      ...(phone && { phone }),
      ...(address && { address })
    });
    newUserProfile.account = newUser;
    await this.profileRepository.save(newUserProfile);
    return { ...newUser, ...newUserProfile };
  }

  async validCurrentPassword({ currentPassword, accountId }: TestCurrentPasswordDto) {
    const user = await this.accountRepository.findOne(accountId);
    if (!user) {
      throw new HttpException("User not found!", HttpStatus.BAD_REQUEST);
    }
    const validPassword = await bcrypt.compare(currentPassword, user.password);
    if (!validPassword) {
      throw new HttpException("Current password do not match", HttpStatus.BAD_REQUEST);
    }
    return user;
  }
}