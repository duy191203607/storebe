import { Strategy } from "passport-jwt";
import { PassportStrategy } from "@nestjs/passport";
import { CACHE_MANAGER, Inject, Injectable } from "@nestjs/common";
import { JwtConstants } from "./constants";
import { Cache } from "cache-manager";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {
    super({
      jwtFromRequest: (req) => {
        if (!req || !req.cookies) return null;
        console.log(req.cookies["access_token"]);
        return req.cookies["access_token"];
      },
      ignoreExpiration: false,
      secretOrKey: JwtConstants.secret
    });
  }

  async validate(data: any): Promise<any> {
    const key = await this.cacheManager.store.keys();
    console.log(key);
    const token = await this.cacheManager.get(`${data.email}_token`);
    return !!token;
  }
}