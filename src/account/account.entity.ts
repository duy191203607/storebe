import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Exclude } from "class-transformer";
import { Profile } from "../profile/profile.entity";

@Entity()
export class Account {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ unique: true, nullable: false, length: 100 })
  email: string;
  @Column({ nullable: false })
  @Exclude()
  password: string;
  @OneToOne(() => Profile, profile => profile.account)
  profile: Profile
}