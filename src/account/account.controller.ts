import {
  Body, CACHE_MANAGER,
  Controller,
  Get,
  HttpException,
  HttpStatus, Inject,
  Post,
  Req, Res,
  UseGuards
} from "@nestjs/common";
import { AccountService } from "./account.service";
import { JwtAuthGuard } from "../auth/jwt.guard";
import { JwtService } from "@nestjs/jwt";
import { Request } from "express";
import { UpdateInfoDto } from "./dto/update-info.dto";
import { Response } from "express";
import { UpdatePasswordDto } from "./dto/update-password.dto";
import { Cache } from "cache-manager";
import { AccessTokenPayload } from "../auth/accessToken.payload";

@Controller("account")
@UseGuards(JwtAuthGuard)
export class AccountController {
  constructor(private readonly accountService: AccountService,
              private readonly jwtService: JwtService,
              @Inject(CACHE_MANAGER) private cacheManager: Cache
  ) {
  }


  @Get("/info")
  async getAccount(@Req() req: Request) {
    try {
      const token: any = this.jwtService.decode(req.cookies["access_token"]);
      return await this.accountService.getAccountInfo(token.id);
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }

  @Post("/update/info")
  async updateInfo(@Body() updateInfoDto: UpdateInfoDto, @Res() res: Response, @Req() req: Request) {
    try {
      const tokenDecode: any = this.jwtService.decode(req.cookies["access_token"]);
      const newAccountInfo = await this.accountService.updateAccountInfo(updateInfoDto, tokenDecode.id);
      const tokenPayload: AccessTokenPayload = {
        id: newAccountInfo.id,
        email: newAccountInfo.email,
        name: newAccountInfo.profile.name
      };
      const newAccessToken = this.jwtService.sign(tokenPayload);
      await this.cacheManager.del(`${tokenDecode.email}_token`);
      await this.cacheManager.set(`${newAccountInfo.email}_token`, newAccessToken, { ttl: 0 });
      res
        .status(200)
        .clearCookie("access_token")
        .cookie("access_token", newAccessToken, {
          httpOnly: true
        })
        .json({
          message: "Update info success",
          data: tokenPayload
        });
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }

  @Post("/update/password")
  async updatePassword(@Body() updatePassword: UpdatePasswordDto, @Res() res: Response, @Req() req: Request) {
    try {
      const tokenDecode: any = this.jwtService.decode(req.cookies["access_token"]);
      await this.accountService.updateAccountPassword(updatePassword, tokenDecode.id);
      await this.cacheManager.del(`${tokenDecode.email}_token`);
      res.status(200).json({
        success: true
      });
      return true;
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }

  @Get("/order")
  async getAccountOrder(@Req() req: Request) {
    try {
      const token: any = this.jwtService.decode(req.cookies["access_token"]);
      return await this.accountService.getAccountOrder(token.id);
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }
}
