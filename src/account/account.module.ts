import { forwardRef, Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Account } from "./account.entity";
import { AccountController } from "./account.controller";
import { AccountService } from "./account.service";
import { OrderModule } from "../order/order.module";
import { ProfileModule } from "../profile/profile.module";

@Module({
  imports: [
    TypeOrmModule.forFeature([Account]),
    forwardRef(() => OrderModule),
    ProfileModule
  ],
  controllers: [AccountController],
  providers: [AccountService],
  exports: [TypeOrmModule]
})
export class AccountModule {
}
