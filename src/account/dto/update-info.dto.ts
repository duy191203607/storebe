import { IsEmail, IsOptional, IsString, MaxLength } from "class-validator";

export class UpdateInfoDto {
  @IsOptional()
  @IsEmail()
  email?: string;
  @IsOptional()
  @IsString()
  name?: string;
  @IsOptional()
  @IsString()
  phone?: string;
  @IsOptional()
  @IsString()
  @MaxLength(50)
  address?: string;
}