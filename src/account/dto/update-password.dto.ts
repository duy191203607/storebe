import { IsNotEmpty, IsString, MaxLength } from "class-validator";

export class UpdatePasswordDto {
  @IsString()
  @MaxLength(255)
  newPassword: string;
}