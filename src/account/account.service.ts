import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Account } from "./account.entity";
import { Repository } from "typeorm";
import { Order, OrderProduct } from "../order/order.entity";
import { UpdateInfoDto } from "./dto/update-info.dto";
import { Profile } from "../profile/profile.entity";
import { UpdatePasswordDto } from "./dto/update-password.dto";
import * as bcrypt from "bcrypt";

@Injectable()
export class AccountService {
  constructor(@InjectRepository(Account) private readonly accountRepository: Repository<Account>,
              @InjectRepository(Order) private readonly orderRepository: Repository<Order>,
              @InjectRepository(Profile) private readonly profileRepository: Repository<Profile>,
              @InjectRepository(OrderProduct) private readonly orderProductRepository: Repository<OrderProduct>) {
  }

  async getAccountInfo(id: number) {
    const user = await this.accountRepository.findOne(id, { relations: ["profile"] });
    if (!user) {
      throw new HttpException("User not found!", HttpStatus.BAD_REQUEST);
    }
    return user;
  }

  async getAccountOrder(id: number) {
    const user = await this.accountRepository.findOne(id);
    if (!user) {
      throw new HttpException("User not found!", HttpStatus.BAD_REQUEST);
    }
    return await this.orderRepository
      .createQueryBuilder("o")
      .innerJoinAndSelect("o.products", "oP")
      .innerJoinAndSelect("oP.product", "p")
      .where("o.accountId = :accountId", { accountId: user.id })
      .getMany();
  }

  async updateAccountInfo({ email, name, phone, address }: UpdateInfoDto, accountId: number): Promise<Account> {
    if (email) {
      const account = await this.accountRepository.findOne({ where: { email } });
      if (account) {
        throw new HttpException("Email already exits!", HttpStatus.BAD_REQUEST);
      } else {
        await this.accountRepository.update(accountId, { email: email });
      }
    }
    if (name || phone || address) {
      await this.profileRepository
        .createQueryBuilder()
        .update(Profile)
        .set({
          ...(name && { name }),
          ...(phone && { phone }),
          ...(address && { address })
        }).execute();
    }
    return await this.accountRepository.findOne(accountId, { relations: ["profile"] });
  }

  async updateAccountPassword({ newPassword }: UpdatePasswordDto, accountId: number): Promise<boolean> {
    const account = await this.accountRepository.findOne(accountId);
    if (!account) {
      throw new HttpException("User not found!", HttpStatus.BAD_REQUEST);
    }
    const hashedNewPassword = await bcrypt.hash(newPassword, 10);
    await this.accountRepository.update(accountId, { password: hashedNewPassword });
    return true;
  }
}
