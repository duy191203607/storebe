import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Product } from "./product.entity";
import { Repository } from "typeorm";
import { ProductQueryParamsDto } from "./dto/product-query-params.dto";
import { AddProductDto } from "./dto/add-product.dto";
import { Category } from "../category/category.entity";
import { Like } from "typeorm";

@Injectable()
export class ProductService {
  constructor(@InjectRepository(Product) private readonly productRepo: Repository<Product>,
              @InjectRepository(Category) private readonly categoryRepo: Repository<Category>) {
  }

  async findById(id: number) {
    return await this.productRepo.findOne(id, { relations: ["category"] });
  }

  async searchByName(name: string) {
    return await this.productRepo.find({
      name: Like(`${name}%`)
    });
  }

  async filter({ page, sort, category, pageSize }: ProductQueryParamsDto) {
    const total = await this.productRepo.count({
      where: {
        ...(category && { category: category })
      }
    });
    const result = await this.productRepo.find({
      where: {
        ...(category && { category: category })
      },
      order: {
        ...(sort !== undefined && { price: sort })
      },
      ...(page ? { skip: (page - 1) * (pageSize ? pageSize : 10), take: (pageSize ? pageSize : 10) } : {})
    });
    return { total, result };
  }

  // async addProduct({ name, price, categoryId, inventory }: AddProductDto) {
  //   const category = await this.categoryRepo.findOne(categoryId);
  //   const newProduct = this.productRepo.create({
  //     name,
  //     price,
  //     inventory,
  //     category
  //   });
  //   await this.productRepo.save(newProduct);
  // }
}