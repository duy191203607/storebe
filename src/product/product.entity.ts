import {
  Column,
  CreateDateColumn,
  Entity, JoinColumn,
  ManyToOne, OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import { Category } from "../category/category.entity";
import { Order, OrderProduct } from "../order/order.entity";

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ nullable: false, type: "varchar", length: "1000", default: "" })
  name: string;
  @Column({ nullable: false, type: "int", default: 0 })
  inventory: number;
  @Column({ nullable: false, type: "float", default: 0 })
  price: number;
  @CreateDateColumn({ name: "created_at" })
  createdAt: Date;
  @UpdateDateColumn({ name: "updated_at" })
  updatedAt: Date;
  @Column({ nullable: true, type: "varchar" })
  image: string;
  @ManyToOne(() => Category, (category) => category.products)
  @JoinColumn({ name: "categoryId" })
  category: Category;
  @OneToMany(() => OrderProduct, (orderProduct) => orderProduct.product, {cascade:true})
  productToOrder!: OrderProduct[];
}