import { IsNotEmpty } from "class-validator";
import { Type } from "class-transformer";

export class SearchParamsDto {
  @IsNotEmpty()
  @Type(() => String)
  name: string;
}