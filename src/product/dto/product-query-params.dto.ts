import { IsIn, IsNotEmpty, IsNumber, IsOptional, Min } from "class-validator";
import { Type } from "class-transformer";

export const SortType = ["ASC", "DESC"];

export class ProductQueryParamsDto {
  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  category?: number;
  @IsOptional()
  @IsIn(SortType)
  @Type(() => String)
  sort?: any;
  @IsNotEmpty()
  @Type(() => Number)
  @Min(1)
  page?: number;
  @Type(() => Number)
  @IsOptional()
  pageSize?: number;
}