import { IsNotEmpty } from "class-validator";

export class AddProductDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  inventory: number;
  @IsNotEmpty()
  price: number;
  @IsNotEmpty()
  categoryId: number;
}