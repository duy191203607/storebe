import { Body, Controller, Get, HttpException, HttpStatus, Param, ParseIntPipe, Post, Query } from "@nestjs/common";
import { ProductService } from "./product.service";
import { ProductQueryParamsDto } from "./dto/product-query-params.dto";
import { AddProductDto } from "./dto/add-product.dto";
import { SearchParamsDto } from "./dto/search-params.dto";

@Controller("product")
export class ProductController {
  constructor(private readonly productService: ProductService) {
  }

  @Get("/search")
  async searchByName(@Query() searchParams: SearchParamsDto) {
    try {
      return await this.productService.searchByName(searchParams.name);
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }

  @Get("/query")
  async filter(@Query() reqParams: ProductQueryParamsDto) {
    try {
      return await this.productService.filter(reqParams);
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }

  // @Post("/create")
  // async addProduct(@Body() addDTO: AddProductDto) {
  //   return await this.productService.addProduct(addDTO);
  // }

  @Get("/:id")
  async findById(@Param("id", new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: number) {
    try {
      return await this.productService.findById(id);
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }
}